# NMLoginModule

[![CI Status](https://img.shields.io/travis/倪文康/NMLoginModule.svg?style=flat)](https://travis-ci.org/倪文康/NMLoginModule)
[![Version](https://img.shields.io/cocoapods/v/NMLoginModule.svg?style=flat)](https://cocoapods.org/pods/NMLoginModule)
[![License](https://img.shields.io/cocoapods/l/NMLoginModule.svg?style=flat)](https://cocoapods.org/pods/NMLoginModule)
[![Platform](https://img.shields.io/cocoapods/p/NMLoginModule.svg?style=flat)](https://cocoapods.org/pods/NMLoginModule)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NMLoginModule is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'NMLoginModule'
```

## Author

倪文康, niwenkang@rxwy.cn

## License

NMLoginModule is available under the MIT license. See the LICENSE file for more info.
