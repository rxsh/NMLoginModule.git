//
//  NMAppDelegate.h
//  NMLoginModule
//
//  Created by 倪文康 on 02/14/2019.
//  Copyright (c) 2019 倪文康. All rights reserved.
//

@import UIKit;

@interface NMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
