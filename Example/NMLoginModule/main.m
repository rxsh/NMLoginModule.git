//
//  main.m
//  NMLoginModule
//
//  Created by 倪文康 on 02/14/2019.
//  Copyright (c) 2019 倪文康. All rights reserved.
//

@import UIKit;
#import "NMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NMAppDelegate class]));
    }
}
